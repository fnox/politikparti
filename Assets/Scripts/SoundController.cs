﻿using UnityEngine;
using System.Collections;

public class SoundController : Singleton<SoundController> {

    public AudioSource Source;

    public enum AnnouncerClip{
        One,
        Two,
        Three,
        Go,
        Game,
        Wins,
        ChooseYourCharacter,
        Stalin,
        Hitler,
        Mao,
        Saddam,
        Elizabeth,
        Ghandi,
        Lincoln,
        Fidel,
        Title
    }

    public AudioClip AnnouncerOne;

    public AudioClip AnnouncerTwo;

    public AudioClip AnnouncerThree;

    public AudioClip AnnouncerGo;

    public AudioClip AnnouncerGame;

    public AudioClip AnnouncerWins;

    public AudioClip AnnouncerSelect;

    public AudioClip AnnouncerStalin;

    public AudioClip AnnouncerGhandi;

    public AudioClip AnnouncerSaddam;

    public AudioClip AnnouncerElizabeth;

    public AudioClip AnnouncerLincoln;

    public AudioClip AnnouncerFidel;

    public AudioClip AnnouncerHitler;

    public AudioClip AnnouncerMao;

    public AudioClip AnnouncerTitle;

    public void PlayAnnouncerClip(AnnouncerClip clip)
    {
        StartCoroutine(PlayAnnouncerVoice(clip));
    }

    public IEnumerator PlayAnnouncerVoice(AnnouncerClip clip)
    {
        switch (clip)
        {
            case AnnouncerClip.Three:
                Source.clip = AnnouncerThree;
                break;
            case AnnouncerClip.Two:
                Source.clip = AnnouncerTwo;
                break;
            case AnnouncerClip.One:
                Source.clip = AnnouncerOne;
                break;
            case AnnouncerClip.Wins:
                Source.clip = AnnouncerWins;
                break;
            case AnnouncerClip.Game:
                Source.clip = AnnouncerGame;
                break;
            case AnnouncerClip.Go:
                Source.clip = AnnouncerGo;
                break;
            case AnnouncerClip.Fidel:
                Source.clip = AnnouncerFidel;
                break;
            case AnnouncerClip.Elizabeth:
                Source.clip = AnnouncerElizabeth;
                break;
            case AnnouncerClip.Hitler:
                Source.clip = AnnouncerHitler;
                break;
            case AnnouncerClip.Lincoln:
                Source.clip = AnnouncerLincoln;
                break;
            case AnnouncerClip.Mao:
                Source.clip = AnnouncerMao;
                break;
            case AnnouncerClip.Ghandi:
                Source.clip = AnnouncerGhandi;
                break;
            case AnnouncerClip.Saddam:
                Source.clip = AnnouncerSaddam;
                break;
            case AnnouncerClip.Stalin:
                Source.clip = AnnouncerStalin;
                break;
            case AnnouncerClip.ChooseYourCharacter:
                Source.clip = AnnouncerSelect;
                break;
            case AnnouncerClip.Title:
                Source.clip = AnnouncerTitle;
                break;
        }
        Source.Play();
        yield return new WaitForSeconds(Source.clip.length);
    }
}
