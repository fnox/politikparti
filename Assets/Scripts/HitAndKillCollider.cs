﻿using UnityEngine;
using System.Collections;

public class HitAndKillCollider : MonoBehaviour {
    public float HitForce = 5f;
    public void OnCollisionEnter(Collision other)
    {
        var thisPlayer = other.gameObject.GetComponent<PlayerController>();
        if (thisPlayer != null)
        {
            thisPlayer.Kill((thisPlayer.transform.position - transform.position + Vector3.up * 0.5f) * HitForce, "collision with kill projectile");
        }
    }
}
