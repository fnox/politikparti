﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using Prime31.TransitionKit;

public class GameController : Singleton<GameController> {

    #region Attributes
    public static string ClientVersion = "0.1";

    public static bool DebugMode = false;

    public static float Gravity = -20f;

    public static int LocalPlayerPort;

    public static int PlayerWonPort;

    public static int PlayerWonStreak;

    public static  bool GameStart = false;

    public static PlayerController.Types PlayerWonChar;

    public static bool[] PlayersReady;

    public static bool[] PlayersActive;

    public static PlayerController.Types[] PlayerTypes;

    public static int MaxTracks;

    public int TrackCount = 0;

    private GameObject PlayerContainer;

    public CameraController CameraCtrl;

    public GameObject CameraCtrlPrefab;

    public GameObject PlayerPrefab;

    public GameObject PortraitHudPrefab;

    public GameObject WorldContainerPrefab;

    public GameObject PlayersContainerPrefab;

    public GameObject HudPrefab;

    public BigTextHUDElement BigText;

    private GameObject PortraitHudContainer;

    public Sprite[] PlayerSprites;

    public List<PlayerController> Players = new List<PlayerController>();
    /// <summary>
    /// List countaining all the diferent platforms
    /// </summary>
    [SerializeField]
    private GameObject[] platformsList;

    /// <summary>
    /// Starting predetermined track
    /// </summary>
    [SerializeField]
    private GameObject startTrack;

    private GameObject startTrackInstance;

    /// <summary>
    /// Finish predetermined track
    /// </summary>
    [SerializeField]
    private GameObject finishTrack;

    /// <summary>
    /// Container to all tracks
    /// </summary>
    private GameObject worldContainer;

    /// <summary>
    /// Last number generated is stored to avoid repeating platforms
    /// </summary>
    private int lastNumberGenerated;

    /// <summary>
    /// Last Item placed is stored to calculate new items position 
    /// </summary>
    private GameObject lastTrackPlaced;

    private StartSegment StartSegmentController;

    private float lastTimePlaced;

    public bool GameWon = false;

    #endregion

    #region Public Methods
    // Use this for initialization
    void Awake()
    {
        if (PlayersActive == null) PlayersActive = new bool[8];
        if (PlayersReady == null) PlayersReady = new bool[8];
        if (PlayerTypes == null) PlayerTypes = new PlayerController.Types[8];
    }

    void Start()
    {
        //if (Application.loadedLevelName == "Game") StartCoroutine(StartGame());
    }

    public void StartGame()
    {
        StartCoroutine(StartGameWorker());
    }

    public System.Collections.IEnumerator StartGameWorker()
    {
        var PlayerCount = 0;
        for (int i=0; i<PlayersActive.Length; i++)
        {
            if (PlayersActive[i]) PlayerCount++;
        }
        MaxTracks = PlayerCount + 2;
        Debug.Log("Player count: " + PlayerCount + " Platform Count: " + MaxTracks);
        //yield return new WaitForSeconds(1.05f);
        GameStart = false;
        GameWon = false;
        TrackCount = 0;
        var CmCtrl = GameObject.FindGameObjectWithTag("CameraContainer");
        CameraCtrl = CmCtrl.GetComponent<CameraController>();
        worldContainer = (GameObject)Instantiate(WorldContainerPrefab);
        PlayerContainer = (GameObject)Instantiate(PlayersContainerPrefab);
        PortraitHudContainer = GameObject.FindGameObjectWithTag("CharacterPortraits");
        BigText = GameObject.FindGameObjectWithTag("BigText").GetComponent<BigTextHUDElement>();
        lastTrackPlaced = null;
        lastNumberGenerated = -1;
        startTrackInstance = PlaceNewPlatform();
        PlaceNewPlatform();
        lastTimePlaced = Time.time;
        StartSegmentController = startTrackInstance.GetComponentInChildren<StartSegment>();
        if (StartSegmentController != null)
        {
            Debug.Log("Creating players");
            for (int i = 0; i < PlayersActive.Length; i++)
            {
                if (!PlayersActive[i])
                {
                    Debug.Log("Player " + i + " is not active");
                }
                else CreatePlayer(i, i % 2 == 0 ? 7 - i : i);
            }
        }
        yield return new WaitForSeconds(2f);
        BigText.ShowBigText("3");
        SoundController.Instance.PlayAnnouncerClip(SoundController.AnnouncerClip.Three);
        yield return new WaitForSeconds(1f);
        BigText.ShowBigText("2");
        SoundController.Instance.PlayAnnouncerClip(SoundController.AnnouncerClip.Two);
        yield return new WaitForSeconds(1f);
        SoundController.Instance.PlayAnnouncerClip(SoundController.AnnouncerClip.One);
        BigText.ShowBigText("1");
        yield return new WaitForSeconds(1f);
        SoundController.Instance.PlayAnnouncerClip(SoundController.AnnouncerClip.Go);
        BigText.ShowBigText("GO!");
        GameStart = true;
        Debug.Log("Game has started");
    }

    private void CreatePlayer(int pos, int spawnposition)
    {
        Debug.Log("Creating Player " + PlayerTypes[pos].ToString());
        var ply = (GameObject)Instantiate(PlayerPrefab, StartSegmentController.SpawnPositions[spawnposition].position, StartSegmentController.SpawnPositions[spawnposition].rotation);
        var plyctrl = ply.GetComponent<PlayerController>();
        plyctrl.Assign(pos + 1, PlayerTypes[pos]);
        Players.Add(plyctrl);
        var plyhud = (GameObject)Instantiate(PortraitHudPrefab, Vector3.zero, Quaternion.Euler(Vector3.zero));
        plyhud.GetComponent<PlayerHudElement>().Assign(plyctrl);
        plyhud.transform.SetParent(PortraitHudContainer.transform);
        ply.transform.SetParent(PlayerContainer.transform);
        plyhud.transform.localScale = Vector3.one;
    }

    // Update is called once per frame
    void Update()
    {
        if (DebugMode || Application.loadedLevelName == "Game")
        {
            if (GameStart && !GameWon && !DebugMode)
            {
                var AlivePlayers = from ply in Players where ply.Alive select ply;
                if (AlivePlayers.Count() == 1) Win(AlivePlayers.First(), "being the last survivor");
                if (AlivePlayers.Count() == 0) Draw();
            }
            CalculatePlayerPositions();
        }
    }

    public void CalculatePlayerPositions()
    {
        foreach (var ply in Players)
        {
            ply.Position = Players.Count;
        }
        for (int i=0; i<Players.Count; i++)
        {
            var ply = Players[i];
            var screenpos = CameraCtrl.CameraObject.WorldToScreenPoint(new Vector3(ply.transform.position.x, 0f, ply.transform.position.z));
            for (int j=0; j<Players.Count; j++)
            {
                var ply2 = Players[j];
                if (i == j || ply.Position > ply2.Position) continue;
                var screenpos2 = CameraCtrl.CameraObject.WorldToScreenPoint(new Vector3(ply2.transform.position.x, 0f, ply2.transform.position.z));
                if ((!ply.Alive && ply2.Alive) || screenpos2.y > screenpos.y || ((!ply.Alive && !ply2.Alive ) && (ply2.DeathTime < ply.DeathTime)) || ply2.PortNumber < ply.PortNumber) ply.Position--;
            }
        }
    }

    /// <summary>
    /// Instantiates a new platform in the World
    /// </summary>
    public GameObject PlaceNewPlatform()
    {
        GameObject newTracktoPlace;
        if (lastTrackPlaced == null)
            lastTrackPlaced = (GameObject)Instantiate(startTrack, worldContainer.transform.position, worldContainer.transform.rotation);
        else if (TrackCount < MaxTracks)
        {
            newTracktoPlace = (GameObject)Instantiate(platformsList[GetRandomPosition()], worldContainer.transform.position, worldContainer.transform.rotation);
            newTracktoPlace.transform.position = CreateNewPosition(newTracktoPlace);
            lastTrackPlaced = newTracktoPlace;
            TrackCount++;
        }
        else if (TrackCount == MaxTracks)
        {
            newTracktoPlace = (GameObject)Instantiate(finishTrack, worldContainer.transform.position, worldContainer.transform.rotation);
            newTracktoPlace.transform.position = CreateNewPosition(newTracktoPlace);
            lastTrackPlaced = newTracktoPlace;
        }
        lastTrackPlaced.transform.SetParent(worldContainer.transform);
        return lastTrackPlaced;
        //TurnOnColliders();
    }

    public bool PlayerTypeAvailable(int port, PlayerController.Types type)
    {
        for (int i=0; i<PlayersReady.Length; i++)
        {
            if (i == port - 1) continue;
            if (PlayersReady[i] && PlayerTypes[i] == type) return false;
        }
        return true;
    }

    public void Win(PlayerController ply, string reason = "")
    {
        Debug.Log("Player " + ply.PortNumber + " has won " + (reason != "" ? "by " + reason + "!" : "!"));
        GameWon = true;
        if (!DebugMode) StartCoroutine(GameWonTransition(ply)); 
    }

    public void Draw() {
        Debug.Log("The game was drawn. Holy fuck.");
        Win(Players[0], "due to sheer luck");
        GameWon = true;
    }

    public System.Collections.IEnumerator GameWonTransition(PlayerController ply)
    {
        if (ply.PortNumber != PlayerWonPort) PlayerWonStreak = 1;
        else PlayerWonStreak++;
        PlayerPrefs.SetString("WinnerType", PlayerWonChar.ToString());
        PlayerPrefs.Save();
        PlayerWonPort = ply.PortNumber;
        PlayerWonChar = ply.CharacterType;
        BigText.ShowBigText("GAME!");
        SoundController.Instance.PlayAnnouncerClip(SoundController.AnnouncerClip.Game);
        yield return new WaitForSeconds(2f);
        SceneSwitchController.Instance.TransitionToWinScreen();
    }
    #endregion


    #region Private Methods
    /// <summary>
    /// Generates a random number between 0 and the number of tracks available
    /// </summary>
    /// <returns>Randon number diferent to last one generated</returns>
    private int GetRandomPosition()
    {
        float fullLenght = (float)platformsList.Length - 0.01f;
        int positiontoUse = (int)Random.Range(0f, fullLenght);
        if (lastNumberGenerated == positiontoUse)
            return GetRandomPosition();
        else
            return lastNumberGenerated = positiontoUse;
    }

    /// <summary>
    /// Generates the position of the newly created track
    /// </summary>
    /// <param name="newTrack">New Track Instance</param>
    /// <returns>New tracks position</returns>
    private Vector3 CreateNewPosition(GameObject newTrack)
    {
        Vector3 newPos = new Vector3();
        newPos = lastTrackPlaced.GetComponent<TrackSegment>().endPoint.position;
        newPos = newPos + (newTrack.transform.position - newTrack.GetComponent<TrackSegment>().startPoint.position);
        return newPos;
    }

    private void TurnOnColliders()
    {
        foreach (Collider col in lastTrackPlaced.GetComponentsInChildren<Collider>())
        {
            col.enabled = true;
        }
    }
    #endregion
}
