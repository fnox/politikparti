﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCollisionTrigger : MonoBehaviour {

    public PlayerController Player;

    public HashSet<PlayerController> OtherPlayers = new HashSet<PlayerController>();

    public void OnTriggerEnter(Collider other)
    {
        var thisPlayer = other.GetComponent<PlayerController>();
        if (thisPlayer != null && thisPlayer.PortNumber != Player.PortNumber)
            OtherPlayers.Add(thisPlayer);
    }

    public void OnTriggerExit(Collider other)
    {
        var thisPlayer = other.GetComponent<PlayerController>();
        if (thisPlayer != null)
            OtherPlayers.Remove(thisPlayer);
    }

}
