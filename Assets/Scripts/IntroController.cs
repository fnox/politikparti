﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IntroController : MonoBehaviour {

    public Text LogoText;

	// Use this for initialization
	public IEnumerator Start () {
        yield return new WaitForSeconds(1.5f);
        LogoText.enabled = true;
        yield return new WaitForSeconds(2f);
        LogoText.canvasRenderer.SetAlpha(1.0f);
        LogoText.CrossFadeAlpha(0f, 0.5f, false);
        yield return new WaitForSeconds(0.6f);
        SceneSwitchController.Instance.TransitionToMainMenu();
	}
	
}
