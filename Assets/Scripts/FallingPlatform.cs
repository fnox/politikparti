﻿using UnityEngine;
using System.Collections;

public class FallingPlatform : MonoBehaviour {

    #region Attributes

    /// <summary>
    /// Time to start falling
    /// </summary>
    [SerializeField]
    private float timetoFall;

    /// <summary>
    /// Shake vibration offset;
    /// </summary>
    [SerializeField]
    private float shakeOffset;

    /// <summary>
    /// Velocity at which the platform shakes.
    /// </summary>
    [SerializeField]
    private float shakeSpeed;

    /// <summary>
    /// Speed at which the platform will pummel to the abyss.
    /// </summary>
    [SerializeField]
    private float fallSpeed;

    /// <summary>
    /// Is the platform falling or about to fall
    /// </summary>
    private bool _isFalling;

    /// <summary>
    /// Is the platform shaking to notify it is about to start falling;
    /// </summary>
    private bool _isShaking;

    /// <summary>
    /// Is going to the right (true) or comming to the left (false)
    /// </summary>
    private bool _isGoing;
    
    /// <summary>
    /// Time in which the platform stared the wait to fall;
    /// </summary>
    private float startedFalling;

    /// <summary>
    /// Controls the shake movement;
    /// </summary>
    private float shakeLerp;

    /// <summary>
    ///  Shake postion 1
    /// </summary>
    private Vector3 rightShakePos;

    /// <summary>
    ///  Shake postion 1
    /// </summary>
    private Vector3 LeftShakePos;

    

    #endregion
    // Use this for initialization
    void Start () {
        _isFalling = false;
        _isShaking = false;
        _isGoing = true;
        rightShakePos = transform.parent.position + Vector3.right * shakeOffset;
        LeftShakePos = transform.parent.position - Vector3.right * shakeOffset;
        shakeLerp = 0.5f;
    }
	
	// Update is called once per frame
	void Update () {
        if (_isFalling != _isShaking)
            FallingLogic();
        else if (_isFalling && _isFalling == _isShaking)
            Fall();

            
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent.tag == "Player")
        {
            _isFalling = true;
            startedFalling = Time.time;
        }
    }

    #region Private Methods
    /// <summary>
    /// Starts the destroy procedure
    /// </summary>
    private void FallingLogic()
    {
        if (_isFalling && !_isShaking)
        {
            _isFalling = false;
            _isShaking = true;
        }
        else if (_isShaking)
        {
            Shake();
            if (Time.time - startedFalling > timetoFall)
            {
                _isFalling = true;
            }
        }
    }
    /// <summary>
    /// Shaking logic
    /// </summary>
    private void Shake()
    {
        this.transform.parent.position = Vector3.Lerp(rightShakePos, LeftShakePos, shakeLerp);
        shakeLerp = _isGoing ? shakeLerp + Time.deltaTime * shakeSpeed : shakeLerp - Time.deltaTime * shakeSpeed;
        if (shakeLerp > 1 || shakeLerp < 0)
        {
            _isGoing = !_isGoing;
            if (shakeLerp > 1)
                shakeLerp = 1;
            else if (shakeLerp < 0)
                shakeLerp = 0;
        }
            
    }

    private void Fall()
    {
        this.transform.parent.position += Vector3.down * Time.deltaTime * fallSpeed;
        if (transform.parent.position.y < -10)
            Destroy(this.transform.parent.gameObject);

    }
    #endregion

}
