﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CharacterSelectionElement : MonoBehaviour {

    public AudioSource Source;
    public int PortNumber;
    public PlayerController.Types Type;
    public Text JoinInText;
    public GameObject EnabledContainer;
    public Image BorderBackground;
    public Image CircleBackground;
    public Image CharacterPortraitBackground;
    public Text PortNumberText;
    public Image CharacterPortrait;
    public Text NotAvailableText;
    public bool ReadyButtonSelected;
    public Text ReadyText;
    public Button ReadyButton;
    public Button QuitButton;
    private bool CanSwitchAgain = false;

    public Sprite MaoSprite;
    public Sprite LincolnSprite;
    public Sprite FidelSprite;
    public Sprite StalinSprite;
    public Sprite ElizabethSprite;
    public Sprite HitlerSprite;
    public Sprite SaddamSprite;
    public Sprite GhandiSprite;


    public Sprite SaddamFace;
    public Sprite HitlerFace;
    public Sprite ElizabethFace;
    public Sprite StalinFace;
    public Sprite FidelFace;
    public Sprite GandhiFace;
    public Sprite LincolnFace;
    public Sprite MaoFace;

    // Use this for initialization
    void Start () {
        PortNumberText.text = "" + PortNumber;
	    switch (PortNumber)
        {
            case 1:
                SetColors(Color.red);
                break;
            case 2:
                SetColors(Color.blue);
                break;
            case 3:
                SetColors(Color.yellow);
                break;
            case 4:
                SetColors(Color.green);
                break;
            case 5:
                SetColors(Color.magenta);
                break;
            case 6:
                SetColors(Color.cyan);
                break;
            case 7:
                SetColors(new Color(1, 0, 1));
                break;
            case 8:
                SetColors(Color.white);
                break;
        }
	}

    public void SetPlayerSprite()
    {
        switch (Type)
        {
            case (PlayerController.Types.Adolf):
                CharacterPortrait.sprite = HitlerSprite;
                CharacterPortraitBackground.sprite = HitlerFace;
                break;
            case (PlayerController.Types.Elizabeth):
                CharacterPortrait.sprite = ElizabethSprite;
                CharacterPortraitBackground.sprite = ElizabethFace;
                break;
            case (PlayerController.Types.Fidel):
                CharacterPortrait.sprite = FidelSprite;
                CharacterPortraitBackground.sprite = FidelFace;
                break;
            case (PlayerController.Types.Ghandi):
                CharacterPortrait.sprite = GhandiSprite;
                CharacterPortraitBackground.sprite = GandhiFace;
                break;
            case (PlayerController.Types.Lincoln):
                CharacterPortrait.sprite = LincolnSprite;
                CharacterPortraitBackground.sprite = LincolnFace;
                break;
            case (PlayerController.Types.Mao):
                CharacterPortrait.sprite = MaoSprite;
                CharacterPortraitBackground.sprite = MaoFace;
                break;
            case (PlayerController.Types.Saddam):
                CharacterPortrait.sprite = SaddamSprite;
                CharacterPortraitBackground.sprite = SaddamFace;
                break;
            case (PlayerController.Types.Stalin):
                CharacterPortrait.sprite = StalinSprite;
                CharacterPortraitBackground.sprite = StalinFace;
                break;
        }
    }

    ///sigh
    public PlayerController.Types NextType()
    {
        switch (Type)
        {
            case PlayerController.Types.Saddam:
                return PlayerController.Types.Ghandi;
            case PlayerController.Types.Ghandi:
                return PlayerController.Types.Elizabeth;
            case PlayerController.Types.Elizabeth:
                return PlayerController.Types.Adolf;
            case PlayerController.Types.Adolf:
                return PlayerController.Types.Mao;
            case PlayerController.Types.Mao:
                return PlayerController.Types.Stalin;
            case PlayerController.Types.Stalin:
                return PlayerController.Types.Lincoln;
            case PlayerController.Types.Lincoln:
                return PlayerController.Types.Fidel;
            case PlayerController.Types.Fidel:
                return PlayerController.Types.Saddam;
            
        }
        return PlayerController.Types.Saddam;
    }

    public PlayerController.Types PrevType()
    {
        switch (Type)
        {
            case PlayerController.Types.Saddam:
                return PlayerController.Types.Fidel;
            case PlayerController.Types.Ghandi:
                return PlayerController.Types.Saddam;
            case PlayerController.Types.Elizabeth:
                return PlayerController.Types.Ghandi;
            case PlayerController.Types.Adolf:
                return PlayerController.Types.Elizabeth;
            case PlayerController.Types.Mao:
                return PlayerController.Types.Adolf;
            case PlayerController.Types.Stalin:
                return PlayerController.Types.Mao;
            case PlayerController.Types.Lincoln:
                return PlayerController.Types.Stalin;
            case PlayerController.Types.Fidel:
                return PlayerController.Types.Lincoln;

        }
        return PlayerController.Types.Saddam;
    }

    public void QuitAction()
    {
        GameController.PlayersActive[PortNumber - 1] = false;
        GameController.PlayersReady[PortNumber - 1] = false;
    }

    public void ReadyAction() {
        if (GameController.Instance.PlayerTypeAvailable(PortNumber, Type))
        {
            GameController.PlayersReady[PortNumber - 1] = !GameController.PlayersReady[PortNumber - 1];
            Source.Play();
        }
        else
            GameController.PlayersReady[PortNumber - 1] = false;
    }

    public void SetColors(Color color)
    {
        BorderBackground.color = color;
        CircleBackground.color = color;
    }
	
	// Update is called once per frame
	void Update () {
        SetPlayerSprite();
        EnabledContainer.SetActive(GameController.PlayersActive[PortNumber - 1]);
        JoinInText.enabled = !GameController.PlayersActive[PortNumber - 1];
        ReadyText.enabled = GameController.PlayersReady[PortNumber - 1];
        NotAvailableText.enabled = !GameController.Instance.PlayerTypeAvailable(PortNumber, Type);
        ReadyButton.interactable = ReadyButtonSelected;
        QuitButton.interactable = !ReadyButtonSelected;
        Type = GameController.PlayerTypes[PortNumber - 1];
        if (GameController.PlayersActive[PortNumber - 1])
        {
            if (Input.GetAxis("Vertical" + PortNumber) > 0.9f) ReadyButtonSelected = true;
            if (Input.GetAxis("Vertical" + PortNumber) < -0.9f) ReadyButtonSelected = false;
            if (Input.GetAxis("Horizontal" + PortNumber) > 0.9f && CanSwitchAgain && !GameController.PlayersReady[PortNumber - 1])
            {
                Type = NextType();
                GameController.PlayerTypes[PortNumber - 1] = Type;
                CanSwitchAgain = false;
            }
            if (Input.GetAxis("Horizontal" + PortNumber) < -0.9f && CanSwitchAgain && !GameController.PlayersReady[PortNumber - 1])
            {
                Type = PrevType();
                GameController.PlayerTypes[PortNumber - 1] = Type;
                CanSwitchAgain = false;
            }
            if (Mathf.Abs(Input.GetAxis("Horizontal" + PortNumber)) <= 0.2f)
            {
                CanSwitchAgain = true;
            }
            if (Input.GetButtonDown("Action" + PortNumber)){
                if (ReadyButtonSelected)
                    ReadyAction();
                else
                    QuitAction();
            }
        }
        if (!GameController.PlayersActive[PortNumber - 1] && Input.GetButton("Action" + PortNumber) && Input.GetButton("Jump" + PortNumber))
            GameController.PlayersActive[PortNumber - 1] = true;
    }
}
