﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour {

    public enum Types
    {
        Saddam,
        Ghandi,
        Elizabeth,
        Adolf,
        Mao,
        Stalin,
        Lincoln,
        Fidel
    }

    public AudioClip JumpSound;

    public AudioClip StepSound;

    public AudioClip DieSound;

    public AudioClip ShoveSound;

    public AudioSource Source;

    public AnimatorOverrideController MaoAnimator;

    public AnimatorOverrideController SaddamAnimator;

    public AnimatorOverrideController GhandiAnimator;

    public AnimatorOverrideController ElizabethAnimator;

    public AnimatorOverrideController AdolfAnimator;

    public AnimatorOverrideController StalinAnimator;

    public AnimatorOverrideController LincolnAnimator;

    public AnimatorOverrideController FidelAnimator;

    public Types CharacterType;

    public int Position;

    public float DeathTime = 0f;

    /// <summary>
    /// Controller number assigned to this player.
    /// </summary>
    public int PortNumber;

    public float Speed;

    public float XDampening;

    public float YDampening;

    public float BackwardMotionDampening;

    public float JumpTime;

    public float JumpForce;

    public bool OnGround;

    public float ShoveForce;

    public SpriteRenderer CharacterSprite;

    public Animator CharacterAnimator;

    public Rigidbody CharacterRigidbody;

    public CharacterController CharacterControl;

    public PlayerCollisionTrigger LeftTrigger;

    public PlayerCollisionTrigger RightTrigger;

    public Vector3 LastPos;

    public Vector3 MovementVector;

    public delegate void PlayerAction();

    public PlayerAction PlayerActionDelegate;

    public delegate void PlayerDeath();

    public PlayerDeath PlayerDeathDelegate;

    public bool InActionZone;

    public GameObject ActivePlatform;

    protected bool CanMove = true;

    protected bool IsShoving = false;

    protected float LastShove = 0f;

    public bool Alive = true;

    public SpriteRenderer Render;

    public float XVelocity = 0f;

    public float YVelocity = 0f;

    public float ZVelocity = 0f;

    protected bool FacingForward;

    protected bool FacingRight;

    protected bool FacingLeft;

    protected bool FacingBack;

    private Vector3 LastPlatformPosition;

    public Vector3 PlatformDelta;


    public void PlayJumpSound()
    {
        Source.clip = JumpSound;
        Source.Play();
    }

    public void PlayStepSound()
    {
        Source.clip = StepSound;
        Source.Play();
    }

    public void PlayDieSound()
    {
        Source.clip = DieSound;
        Source.Play();
    }

    public void PlayShoveSound()
    {
        Source.clip = ShoveSound;
        Source.Play();
    }

    public void Assign(int portnumber, Types type)
    {
        PortNumber = portnumber;
        CharacterType = type;
        switch (type)
        {
            case Types.Saddam:
                CharacterAnimator.runtimeAnimatorController = SaddamAnimator;
                break;
            case Types.Ghandi:
                CharacterAnimator.runtimeAnimatorController = GhandiAnimator;
                break;
            case Types.Elizabeth:
                CharacterAnimator.runtimeAnimatorController = ElizabethAnimator;
                break;
            case Types.Mao:
                CharacterAnimator.runtimeAnimatorController = MaoAnimator;
                break;
            case Types.Stalin:
                CharacterAnimator.runtimeAnimatorController = StalinAnimator;
                break;
            case Types.Fidel:
                CharacterAnimator.runtimeAnimatorController = FidelAnimator;
                break;
            case Types.Adolf:
                CharacterAnimator.runtimeAnimatorController = AdolfAnimator;
                break;
            case Types.Lincoln:
                CharacterAnimator.runtimeAnimatorController = LincolnAnimator;
                break;
        }
    }

    public void SetColor(Color color)
    {
        CharacterSprite.color = color;
    }

    public void ResetColor()
    {
        CharacterSprite.color = Color.white;
    }

    public IEnumerator MovementRoutine()
    {
        while (!GameController.Instance.GameWon)
        {
            LastPos = transform.position;
            if (CanMove && Alive && GameController.GameStart) {
                XVelocity = Speed * Input.GetAxis("Horizontal" + PortNumber) * XDampening;
                ZVelocity = Mathf.Clamp(Speed * Input.GetAxis("Vertical" + PortNumber) * YDampening, YDampening * -Speed, YDampening * Speed * BackwardMotionDampening);
                var MovementVector = new Vector3(XVelocity, YVelocity, ZVelocity);
            }
            CharacterControl.Move(transform.TransformDirection(new Vector3(XVelocity, YVelocity, ZVelocity) * Time.fixedDeltaTime + PlatformDelta));
            CharacterAnimator.SetBool("facef", Mathf.RoundToInt(ZVelocity) < 0);
            CharacterAnimator.SetBool("faced", Mathf.RoundToInt(ZVelocity) > 0);
            CharacterAnimator.SetBool("facer", Mathf.RoundToInt(XVelocity) < 0);
            CharacterAnimator.SetBool("facel", Mathf.RoundToInt(XVelocity) > 0);
            yield return new WaitForFixedUpdate();
        }
    }

    public void GetShoved(Vector3 Orig)
    {
        StartCoroutine(ShoveRoutine(Orig));
    }

    public IEnumerator ShoveRoutine(Vector3 ForceVector)
    {
        CanMove = false;
        var Timer = 0f;
        ForceVector *= ShoveForce;
        YVelocity = ForceVector.y;
        while (Timer <= 0.5f)
        {
            ForceVector = new Vector3(ForceVector.x * (1f - Timer * 2f), YVelocity, ForceVector.z * (1f - Timer * 2f));
            Timer += Time.fixedDeltaTime;
            XVelocity = ForceVector.x;
            ZVelocity = ForceVector.z;
            yield return new WaitForFixedUpdate();
        }
        yield return new WaitForSeconds(0.5f);
        CanMove = true;

    }

    public void CheckAlive()
    {
        if (!GameController.Instance.CameraCtrl.IsObjectInCameraView(gameObject) && OnGround) Kill("out of camera view");
    }

    public IEnumerator ActionRoutine()
    {
        while (Alive)
        {
            if (Input.GetButton("Action" + PortNumber) && OnGround && CanMove && GameController.GameStart)
            {
                PlayShoveSound();
                CharacterAnimator.SetBool("action", true);
                CharacterRigidbody.velocity = Vector3.zero;
                CanMove = false;
                XVelocity = ZVelocity = 0f;
                if (InActionZone)
                {
                    if (PlayerActionDelegate != null)
                    {
                        PlayerActionDelegate();
                    }
                    yield return null;
                }
                else
                {
                    if (RightTrigger.OtherPlayers.Count > LeftTrigger.OtherPlayers.Count)
                    {
                        foreach (var ply in RightTrigger.OtherPlayers)
                        {
                            if (ply == null || ply.PortNumber == PortNumber) continue;
                            ply.GetShoved(Vector3.left + Vector3.up * 0.2f);
                        }
                    }
                    else
                    {
                        var hits = Physics.SphereCastAll(LeftTrigger.transform.position, 0.5f, Vector3.right);
                        foreach (var ply in LeftTrigger.OtherPlayers)
                        {
                            if (ply == null || ply.PortNumber == PortNumber) continue;
                            ply.GetShoved(Vector3.right + Vector3.up * 0.2f);
                        }
                    }
                    yield return new WaitForSeconds(0.25f);
                }
                CanMove = true;
                CharacterAnimator.SetBool("action", false);
            }
            yield return new WaitForFixedUpdate();
        }
    }

    public IEnumerator JumpingRoutine()
    {
        while (true)
        {
            //god forgive me
            OnGround = false;
            var platform = ActivePlatform;
            var found = false;
            foreach(var hit in Physics.RaycastAll(transform.position, Vector3.down, 0.65f, LayerMask.GetMask("Ground")))
            {
                if (!hit.collider.isTrigger) {
                    OnGround = true;
                    found = true;
                    ActivePlatform = hit.collider.gameObject;
                    break;
                }
            }
            if (!found)
            {
                foreach (var hit in Physics.RaycastAll(transform.position + Vector3.right * 0.5f, Vector3.down, 0.65f, LayerMask.GetMask("Ground")))
                {
                    if (!hit.collider.isTrigger)
                    {
                        OnGround = true;
                        found = true;
                        ActivePlatform = hit.collider.gameObject;
                        break;
                    }
                }
            }
            if (!found)
            {
                foreach (var hit in Physics.RaycastAll(transform.position - Vector3.right * 0.5f, Vector3.down, 0.65f, LayerMask.GetMask("Ground")))
                {
                    if (!hit.collider.isTrigger)
                    {
                        OnGround = true;
                        found = true;
                        ActivePlatform = hit.collider.gameObject;
                        break;
                    }
                }
            }
            if (platform != ActivePlatform)
            {
                PlatformDelta = Vector3.zero;
                LastPlatformPosition = ActivePlatform.transform.position;
            }
            if (OnGround)
            {
                if (Input.GetButton("Jump" + PortNumber) && Alive && GameController.GameStart)
                {
                    PlayJumpSound();
                    OnGround = false;
                    YVelocity = JumpForce;
                }
                else
                    YVelocity = 0f;
            }
            else
            {
                ActivePlatform = null;
                PlatformDelta = Vector3.zero;
                YVelocity = Mathf.Max(GameController.Gravity, YVelocity + GameController.Gravity * Time.fixedDeltaTime);
            }
            CharacterAnimator.SetBool("onground", OnGround);
            yield return new WaitForFixedUpdate();
        }
    }

    public void Kill(string reason = "")
    {
        if (!Alive || GameController.Instance.GameWon) return;
        Debug.Log("Player " + PortNumber + " died" + (reason != "" ? " due to " + reason + "." : "."));
        CharacterAnimator.SetBool("alive", false);
        PlayDieSound();
        Alive = false;
        XVelocity = 0f;
        YVelocity = 0f;
        ZVelocity = 0f;
        DeathTime = Time.time;
        if (PlayerDeathDelegate != null) PlayerDeathDelegate();
    }

    public void Kill(Vector3 KillDirection, string reason = "")
    {
        if (!Alive || GameController.Instance.GameWon) return;
        CharacterAnimator.SetBool("alive", false);
        Debug.Log("Player " + PortNumber + " died" + (reason != "" ? " due to " + reason + "." : "."));
        PlayDieSound();
        Alive = false;
        XVelocity = KillDirection.x;
        YVelocity = KillDirection.y;
        ZVelocity = KillDirection.z;
        DeathTime = Time.time;
        if (PlayerDeathDelegate != null) PlayerDeathDelegate();
    }

    public void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + Vector3.down * (GetComponent<Collider>().bounds.extents.y + 0.01f));
        Gizmos.DrawWireSphere(RightTrigger.transform.position, RightTrigger.GetComponent<Collider>().bounds.extents.x);
        Gizmos.DrawWireSphere(LeftTrigger.transform.position, LeftTrigger.GetComponent<Collider>().bounds.extents.x);
    }

	// Use this for initialization
	IEnumerator Start () {
        while (!GameController.GameStart) yield return null;
        StartCoroutine(MovementRoutine());
        StartCoroutine(ActionRoutine());
        StartCoroutine(JumpingRoutine());
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        CheckAlive();
        if (ActivePlatform != null)
        {
            PlatformDelta = ActivePlatform.transform.position - LastPlatformPosition;
            LastPlatformPosition = ActivePlatform.transform.position;
        }
	}
}
