﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerHudElement : MonoBehaviour {
    public Image PlayerPortrait;
    public Image PortraitBackground;
    public Text PositionText;
    public Text NameText;
    public PlayerController Ply;
    public Image Cross;
    public Animator Anim;

    public Sprite SaddamFace;
    public Sprite HitlerFace;
    public Sprite ElizabethFace;
    public Sprite StalinFace;
    public Sprite FidelFace;
    public Sprite GandhiFace;
    public Sprite LincolnFace;
    public Sprite MaoFace;

    private bool Played = false;

    public void Assign(PlayerController ply)
    {
        Ply = ply;
        NameText.text = Ply.CharacterType.ToString();
        PlayerController.Types type = ply.CharacterType;
        
        switch (type)
                {
                    case PlayerController.Types.Saddam:
                        PlayerPortrait.sprite = SaddamFace;
                        break;
                    case PlayerController.Types.Ghandi:
                        PlayerPortrait.sprite = GandhiFace;
                        break;
                    case PlayerController.Types.Elizabeth:
                        PlayerPortrait.sprite = ElizabethFace;
                        break;
                    case PlayerController.Types.Adolf:
                        PlayerPortrait.sprite = HitlerFace;
                        break;
                    case PlayerController.Types.Mao: 
                        PlayerPortrait.sprite = MaoFace;
                        break;
                    case PlayerController.Types.Stalin:
                        PlayerPortrait.sprite = StalinFace;
                        break;
                    case PlayerController.Types.Lincoln:
                        PlayerPortrait.sprite = LincolnFace;
                        break;
                    case PlayerController.Types.Fidel:
                        PlayerPortrait.sprite = FidelFace;
                        break;
                    default:
                        PlayerPortrait.sprite = null;
                        break;
                }
        switch (Ply.PortNumber)
        {
            case 1:
                PortraitBackground.color = Color.red;
                break;
            case 2:
                PortraitBackground.color = Color.blue;
                break;
            case 3:
                PortraitBackground.color = Color.yellow;
                break;
            case 4:
                PortraitBackground.color = Color.green;
                break;
            case 5:
                PortraitBackground.color = Color.magenta;
                break;
            case 6:
                PortraitBackground.color = Color.cyan;
                break;
            case 7:
                PortraitBackground.color = new Color(1, 0, 1);
                break;
            case 8:
                PortraitBackground.color = Color.white;
                break;
        }
    }
    public void ChangePosition(string text)
    {
        PositionText.text = text;
    }
    public void PlayDieAnimation()
    {
        if (Played) return;
        Anim.Play("hudshake");
        Played = true;
    }
    void Update()
    {
        if (!Ply.Alive)
        {
            PlayDieAnimation();
            Cross.enabled = true;
        }
        ChangePosition("" + Ply.Position);
    }
}
