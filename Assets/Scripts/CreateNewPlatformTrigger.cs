﻿using UnityEngine;
using System.Collections;

public class CreateNewPlatformTrigger : MonoBehaviour {
    private bool Generated = false;
    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            var thisPlayer = other.GetComponent<PlayerController>();
            if (thisPlayer != null && !Generated)
            {
                Generated = true;
                GameController.Instance.PlaceNewPlatform();
            }
        }
    }
}
