﻿using UnityEngine;
using System.Collections;
using Photon;

public class SelectCharacterController : PunBehaviour {
    public bool done = false;
    public GameObject Canvas;

    public bool CheckPlayersReady()
    {
        int plycount = 0;
        for(int i = 0; i < GameController.PlayersReady.Length; i++){
            if (!GameController.PlayersActive[i]) continue;
            if (GameController.PlayersActive[i] && !GameController.PlayersReady[i]) return false;
            else plycount++;
        }
        return plycount > 1;
    }
    
    IEnumerator Start()
    {
        Canvas.SetActive(false);
        for (int i=0; i < GameController.PlayersReady.Length; i++)
        {
            GameController.PlayersReady[i] = false;
        }
        yield return new WaitForSeconds(1f);
        SoundController.Instance.PlayAnnouncerClip(SoundController.AnnouncerClip.ChooseYourCharacter);
        Canvas.SetActive(true);
    }

	// Update is called once per frame
	void Update () {
        for (int i=0; i< PhotonNetwork.room.maxPlayers; i++)
        {
            GameController.PlayersActive[i] = i < PhotonNetwork.room.playerCount;
        }
        if (!done && CheckPlayersReady())
        {
            done = true;
            SceneSwitchController.Instance.TransitiontoGame();
        }
	}
}
