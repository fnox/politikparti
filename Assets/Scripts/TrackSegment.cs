﻿using UnityEngine;
using System.Collections;

public abstract class TrackSegment : MonoBehaviour {

    #region Attributes
    /// <summary>
    /// Point where the segment starts
    /// </summary>
    [SerializeField]
    protected Transform _startPoint;

    /// <summary>
    /// Point where the segment ends
    /// </summary>
    [SerializeField]
    protected Transform _endPoint;
    #endregion

    #region Properties
    /// <summary>
    /// Exposes starPoint as read-only
    /// </summary>
    public Transform startPoint { get { return _startPoint; } }

    /// <summary>
    /// Exposes endPoint as read-only
    /// </summary>
    public Transform endPoint { get { return _endPoint; } }

    #endregion

    #region Public Methods
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        executeLogic();
    }

    /// <summary>
    /// Should this track spawn according to the current conditions of the game?
    /// </summary>
    /// <returns></returns>
    public abstract bool ShouldSpawn();

    /// <summary>
    /// Allows to access the logic located in his children
    /// </summary>
    public abstract void executeLogic();

    #endregion
}
