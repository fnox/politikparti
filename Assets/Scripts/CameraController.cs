﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

    public Camera CameraObject;

    public GameObject LeadPlayer;

    private Plane[] Planes;

    private float PrevDelta;


	// Use this for initialization
	void Start () {
        CameraObject = Camera.main;
        UpdateCameraPlanes();
        GameController.Instance.StartGame();
    }

    private void UpdateCameraPlanes()
    {
        Planes = GeometryUtility.CalculateFrustumPlanes(CameraObject);
    }

    public bool IsObjectInCameraView(GameObject obj)
    {
        if (obj.GetComponent<Collider>() == null) return false;
        var bounds = obj.GetComponent<Collider>().bounds;
        bounds.Expand(obj.transform.localScale.x);
        return GeometryUtility.TestPlanesAABB(Planes, bounds);
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        UpdateCameraPlanes();
        var maxpos = new Vector3(Screen.width, Screen.height/2f, 0f);
        PlayerController maxplayer = null;
        foreach(PlayerController Player in GameController.Instance.Players)
        {
            var pos = CameraObject.WorldToScreenPoint(new Vector3(Player.transform.position.x, 0f, Player.transform.position.z));
            if (pos.y < maxpos.y)
            {
                pos = maxpos;
                maxplayer = Player;
            }
        }
        if (maxplayer != null)
        {
            var delta = Mathf.Min(0f, maxplayer.ZVelocity * Time.fixedDeltaTime);
            transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Lerp(transform.position.z, transform.position.z + delta, 1f));
        }
    }
}
