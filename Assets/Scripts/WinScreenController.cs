﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WinScreenController : MonoBehaviour {

    public Text WinnerText;

    [SerializeField]
    private GameObject[] playersWinScreen;

    [SerializeField]
    private Sprite[] playerSprites;

    public AnimatorOverrideController[] animations;

    private float timePassed = 0.0f;

    [SerializeField]
    private float timeToSkip = 1.0f;

    public AudioClip WinScreenMusic;

    public AudioSource CheerSource;

    public AudioSource MusicSource;

    public Text AnyKeyText;

    public GameObject Light;

    public Animator WinSpriteAnimator;

    // Use this for initialization
    IEnumerator Start () {
        WinnerText.text = Prettify(GameController.PlayerWonChar);
        yield return new WaitForSeconds(1f);
        Light.SetActive(true);
        SetPlayersSprites();
        StartCoroutine(PlayAnnouncer());
        yield return new WaitForSeconds(CheerSource.clip.length - 2f);
        AnyKeyText.canvasRenderer.SetAlpha(0f);
        AnyKeyText.enabled = true;
        AnyKeyText.CrossFadeAlpha(1f, 1f, true);
        MusicSource.Play();
    }
	
    public void SetPlayersSprites ()
    {
        int numPlayers = GameController.PlayersActive.Length;
        SetForWinScreen(playersWinScreen[0], GameController.PlayerWonChar, null);
        AnimateWinner();
        var aux = 1;
        for (int i = 0; i < numPlayers; i++)
        {
            if (i == GameController.PlayerWonPort-1) continue; 
            if (GameController.PlayersActive[i])
            {
                AnimatorOverrideController animAux = null;
                PlayerController.Types type = GameController.PlayerTypes[i];
                Debug.Log("player " + (i+1) + " was " + type.ToString());
                /*switch (type)
                {
                    case PlayerController.Types.Saddam:
                        animAux = animations[0];
                        break;
                    case PlayerController.Types.Ghandi:
                        animAux = animations[1];
                        break;
                    case PlayerController.Types.Elizabeth:
                        animAux = animations[2];
                        break;
                    case PlayerController.Types.Mao:
                        animAux = animations[3];
                        break;
                    case PlayerController.Types.Stalin:
                        animAux = animations[4];
                        break;
                    case PlayerController.Types.Fidel:
                        animAux = animations[5];
                        break;
                    case PlayerController.Types.Adolf:
                        animAux = animations[6];
                        break;
                    case PlayerController.Types.Lincoln:
                        animAux = animations[7];
                        break;
                    default:
                        animAux = null;
                        break;
                }*/
                SetForWinScreen(playersWinScreen[aux++], type, animAux);
            }
        }
    }

    public void AnimateWinner()
    {
        WinSpriteAnimator.Play(GameController.PlayerWonChar.ToString());
    }

    private IEnumerator PlayAnnouncer()
    {
        yield return new WaitForSeconds(1f);
        switch (GameController.PlayerWonChar)
        {
            case PlayerController.Types.Saddam:
                yield return StartCoroutine(SoundController.Instance.PlayAnnouncerVoice(SoundController.AnnouncerClip.Saddam));
                break;
            case PlayerController.Types.Ghandi:
                yield return StartCoroutine(SoundController.Instance.PlayAnnouncerVoice(SoundController.AnnouncerClip.Ghandi));
                break;
            case PlayerController.Types.Elizabeth:
                yield return StartCoroutine(SoundController.Instance.PlayAnnouncerVoice(SoundController.AnnouncerClip.Elizabeth));
                break;
            case PlayerController.Types.Mao:
                yield return StartCoroutine(SoundController.Instance.PlayAnnouncerVoice(SoundController.AnnouncerClip.Mao));
                break;
            case PlayerController.Types.Stalin:
                yield return StartCoroutine(SoundController.Instance.PlayAnnouncerVoice(SoundController.AnnouncerClip.Stalin));
                break;
            case PlayerController.Types.Fidel:
                yield return StartCoroutine(SoundController.Instance.PlayAnnouncerVoice(SoundController.AnnouncerClip.Fidel));
                break;
            case PlayerController.Types.Adolf:
                yield return StartCoroutine(SoundController.Instance.PlayAnnouncerVoice(SoundController.AnnouncerClip.Hitler));
                break;
            case PlayerController.Types.Lincoln:
                yield return StartCoroutine(SoundController.Instance.PlayAnnouncerVoice(SoundController.AnnouncerClip.Lincoln));
                break;
            default:
                break;
        }
        yield return new WaitForSeconds(0.25f);
        SoundController.Instance.PlayAnnouncerClip(SoundController.AnnouncerClip.Wins);
    }

    private void SetForWinScreen (GameObject player, PlayerController.Types type, AnimatorOverrideController anim)
    {
        player.SetActive(true);
        Animator playerAnimator = player.GetComponent<Animator>();
        Debug.Log(playerAnimator != null);
        if (playerAnimator != null)
        {
            //playerAnimator.runtimeAnimatorController = anim;
            SpriteRenderer playerImage = player.GetComponent<SpriteRenderer>();
            if (playerImage != null)
            {
                switch (type)
                {
                    case PlayerController.Types.Saddam:
                        playerImage.sprite = playerSprites[0];
                        break;
                    case PlayerController.Types.Ghandi:
                        playerImage.sprite = playerSprites[1];
                        break;
                    case PlayerController.Types.Elizabeth:
                        playerImage.sprite = playerSprites[2];
                        break;
                    case PlayerController.Types.Mao:
                        playerImage.sprite = playerSprites[3];
                        break;
                    case PlayerController.Types.Stalin:
                        playerImage.sprite = playerSprites[4];
                        break;
                    case PlayerController.Types.Fidel:
                        playerImage.sprite = playerSprites[5];
                        break;
                    case PlayerController.Types.Adolf:
                        playerImage.sprite = playerSprites[6];
                        break;
                    case PlayerController.Types.Lincoln:
                        playerImage.sprite = playerSprites[7];
                        break;
                }
            }
        }
    }

    public string Prettify(PlayerController.Types type)
    {
        switch (type)
        {
            case (PlayerController.Types.Adolf):
                return "Adolf Hitler";
            case (PlayerController.Types.Elizabeth):
                return "Queen Elizabeth";
            case (PlayerController.Types.Fidel):
                return "Fidel Castro";
            case (PlayerController.Types.Ghandi):
                return "Mahatma Gandhi";
            case (PlayerController.Types.Lincoln):
                return "Abraham Lincoln";
            case (PlayerController.Types.Mao):
                return "Mao Zedong";
            case (PlayerController.Types.Saddam):
                return "Saddam Hussein";
            case (PlayerController.Types.Stalin):
                return "Joseph Stalin";
        }
        return "";
    }

	// Update is called once per frame
	void FixedUpdate () {
        timePassed += Time.deltaTime;

        if (timePassed >= timeToSkip && Input.anyKeyDown) SceneSwitchController.Instance.TransitionToCharacterSelect(); 
	}
}
