﻿using UnityEngine;
using System.Collections;

public class CameraFacingBillboard : MonoBehaviour {

	// Update is called once per frame
	void Update () {
        if (GameController.Instance.CameraCtrl != null) transform.LookAt(transform.position + GameController.Instance.CameraCtrl.transform.rotation * Vector3.forward);
    }
}
