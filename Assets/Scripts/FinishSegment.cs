﻿using UnityEngine;
using System.Collections;
using System;

public class FinishSegment : TrackSegment {

    #region Attributes
    /// <summary>
    /// Collider activates the win condition
    /// </summary>
    [SerializeField]
    private BoxCollider _winCollider;
    #endregion

    #region Properties
    /// <summary>
    /// Exposes the _winCollider as read-only
    /// </summary>
    public BoxCollider winCollider { get { return _winCollider; } }
    #endregion

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public override void executeLogic()
    {

    }

    public override bool ShouldSpawn()
    {
        return true;
    }
}
