﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using Photon;

public class MainMenuController : PunBehaviour {
    // Update is called once per frame

    public Sprite SaddamFace;
    public Sprite HitlerFace;
    public Sprite ElizabethFace;
    public Sprite StalinFace;
    public Sprite FidelFace;
    public Sprite GandhiFace;
    public Sprite LincolnFace;
    public Sprite MaoFace;
    public Image WinnerSprite;
    public Text StatusLabel;

    IEnumerator Start()
    {
        var winner = PlayerPrefs.GetString("WinnerType", "None");
        switch (winner)
        {
            case ("Ghandi"):
                WinnerSprite.sprite = GandhiFace;
                break;
            case ("Adolf"):
                WinnerSprite.sprite = HitlerFace;
                break;
            case ("Elizabeth"):
                WinnerSprite.sprite = ElizabethFace;
                break;
            case ("Stalin"):
                WinnerSprite.sprite = StalinFace;
                break;
            case ("Lincoln"):
                WinnerSprite.sprite = LincolnFace;
                break;
            case ("Mao"):
                WinnerSprite.sprite = MaoFace;
                break;
            case ("Fidel"):
                WinnerSprite.sprite = FidelFace;
                break;
            case ("Saddam"):
                WinnerSprite.sprite = SaddamFace;
                break;
            default:
                WinnerSprite.enabled = false;
                break;
        }
        yield return new WaitForSeconds(1f);
        SoundController.Instance.PlayAnnouncerClip(SoundController.AnnouncerClip.Title);
        PhotonNetwork.autoJoinLobby = true;
        PhotonNetwork.ConnectUsingSettings(GameController.ClientVersion);
    }

    public override void OnConnectionFail(DisconnectCause cause)
    {
        base.OnConnectionFail(cause);
    }

    public override void OnJoinedLobby()
    {
        base.OnJoinedLobby();
        Debug.Log("Connected to lobby");
        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnPhotonRandomJoinFailed(object[] codeAndMsg)
    {
        base.OnPhotonRandomJoinFailed(codeAndMsg);
        Debug.Log("Couldn't join random lobby, creating one");
        var options = new RoomOptions();
        options.maxPlayers = 8;
        options.isOpen = true;
        options.isVisible = true;
        PhotonNetwork.JoinOrCreateRoom("ROOM_" + Random.Range(1, 99), options, PhotonNetwork.lobby);
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("Room joined");
        SceneSwitchController.Instance.TransitionToCharacterSelect();
    }

    void Update () {
        StatusLabel.text = PhotonNetwork.connectionState.ToString();
        //if (Input.anyKeyDown) SceneSwitchController.Instance.TransitionToCharacterSelect(); 
	}
}
