﻿using UnityEngine;
using System.Collections;

public class TriviaStartController : MonoBehaviour {

    [SerializeField]
    TriviaTrack triviaTrackController;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerStay(Collider other)
    {
        if (!triviaTrackController.startLogic)
        {
            if (other.tag == "Player")
            {
                triviaTrackController.startLogic = true;
                triviaTrackController.triviaStartTimeLogic = Time.time;
                this.transform.gameObject.SetActive(false);
            }
        }
    }
}
