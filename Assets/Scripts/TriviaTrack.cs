﻿using UnityEngine;
using System.Collections;
using System;

public struct TriviaQuestionObject
{
    public TriviaQuestionObject(string question, string rAnswer, string lAnswer, bool cAnswer)
    {
        questionText = question;
        rightAnswerText = rAnswer;
        leftAnswerText = lAnswer;
        correctAnswer = cAnswer;
    }

    public string questionText;

    public string rightAnswerText;

    public string leftAnswerText;

    public bool correctAnswer; //true: right is correct, false: left is correct
}

public class TriviaQuestionList
{
    private TriviaQuestionObject[] triviaArray = new TriviaQuestionObject[8];

    public TriviaQuestionList()
    {
        int i = 0;
        triviaArray[i++] = new TriviaQuestionObject("On which month did the october revolution take place?", "October", "November", false);
        triviaArray[i++] = new TriviaQuestionObject("Where did the Night of the Long Knives take place?", "Germany", "Russia", true);
        triviaArray[i++] = new TriviaQuestionObject("Which political party did Abraham Lincoln belong to?", "Republicans", "Democrats", true);
        triviaArray[i++] = new TriviaQuestionObject("On which year did the Cuban Missile Crisis take place?", "1965", "1962", false);
        triviaArray[i++] = new TriviaQuestionObject("What is Ghandi's first name?", "Mahatma", "Mohandas", true);
        triviaArray[i++] = new TriviaQuestionObject("The Great Leap Forward was a campaign started by:", "Hitler", "Mao", false);
        triviaArray[i++] = new TriviaQuestionObject("Who was Queen Elizabeth II's father?", "George IV", "Henry VII", true);
        triviaArray[i++] = new TriviaQuestionObject("Saddam Hussein was the ruler of which of these countries?", "Iran", "Iraq", false);
    }

    public TriviaQuestionObject GetRandomQuestion()
    {
        float max = triviaArray.Length - 0.01f;
        int questionToUse = (int)UnityEngine.Random.Range(0f, max);
        return triviaArray[questionToUse];
    }

}

public class TriviaTrack : TrackSegment {

    [SerializeField]
    private TextMesh questionText;

    [SerializeField]
    private TextMesh rightAnswerText;

    [SerializeField]
    private TextMesh leftAnswerText;

    [SerializeField]
    private GameObject rightBarrier;

    [SerializeField]
    private GameObject leftBarrier;

    [SerializeField]
    private float timetoAnswer;

    [SerializeField]
    private BoxCollider startCol;

    [SerializeField]
    private BoxCollider rightCol;

    [SerializeField]
    private BoxCollider leftCol;

    private float _triviaStartTimeLogic;

    private bool correctAnswer; //true: right is correct, false: left is correct

    private bool _startLogic;

    public bool startLogic { get { return _startLogic; } set { _startLogic = value; } }

    public float triviaStartTimeLogic { get { return _triviaStartTimeLogic; } set { _triviaStartTimeLogic = value; } }



    // Use this for initialization
    void Start () {
        TriviaQuestionObject trivia = new TriviaQuestionList().GetRandomQuestion();
        questionText.text = trivia.questionText;
        rightAnswerText.text = trivia.rightAnswerText;
        leftAnswerText.text = trivia.leftAnswerText;
        correctAnswer = trivia.correctAnswer;
        rightBarrier.SetActive(false);
        leftBarrier.SetActive(false);
        _startLogic = false;
        if (correctAnswer)
            leftCol.enabled = false;
        else
            rightCol.enabled = false;
        _triviaStartTimeLogic = 0;
    }
	
	// Update is called once per frame
	void Update () {
        if (_startLogic)
        {
            if (Time.time - _triviaStartTimeLogic > timetoAnswer)
            {
                turnOffFailColliders();
                _startLogic = false;
            }
        }
    }

    public override void executeLogic()
    {
        throw new NotImplementedException();
    }

    public override bool ShouldSpawn()
    {
        throw new NotImplementedException();
    }

    private void turnOffFailColliders()
    {
        startCol.enabled = false;
        if (correctAnswer)
            leftCol.enabled = false;
        else
            rightCol.enabled = false;
        StartCoroutine(turnOffAdvanceCols());
    }

    private IEnumerator turnOffAdvanceCols()
    {
        yield return new WaitForSeconds(0.2f);
        rightBarrier.SetActive(false);
        leftBarrier.SetActive(false);
        yield return null;
    }
}
