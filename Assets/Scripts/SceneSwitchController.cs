﻿using UnityEngine;
using System.Collections;
using Prime31.TransitionKit;

public class SceneSwitchController : Singleton<SceneSwitchController> {

	public void TransitionToWinScreen()
    {
        Debug.Log("Transitioning to Win Screen");
        var wind = new WindTransition()
        {
            nextScene = 4,
            duration = 1.0f,
            size = 0.3f
        };
        TransitionKit.instance.transitionWithDelegate(wind);
    }

    public void TransitionToMainMenu()
    {
        Debug.Log("Transitioning to Main Menu");
        var wind = new WindTransition()
        {
            useCurvedWind = true,
            nextScene = 1,
            duration = 1.0f,
            size = 0.3f,
            windVerticalSegments = 300f
        };
        TransitionKit.instance.transitionWithDelegate(wind);
    }

    public void TransitionToCharacterSelect()
    {
        Debug.Log("Transitioning to Character Select");
        var squares = new SquaresTransition()
        {
            nextScene = 2,
            duration = 1.0f,
            squareSize = new Vector2(5f, 4f),
            smoothness = 0.0f
        };
        TransitionKit.instance.transitionWithDelegate(squares);
    }

    public void TransitiontoGame()
    {
        Debug.Log("Transitioning to Game");
        var enumValues = System.Enum.GetValues(typeof(PixelateTransition.PixelateFinalScaleEffect));
        var randomScaleEffect = (PixelateTransition.PixelateFinalScaleEffect)enumValues.GetValue(Random.Range(0, enumValues.Length));
        var wind = new WindTransition()
        {
            nextScene = 3,
            duration = 1.0f,
            size = 0.3f,
        };
        TransitionKit.instance.transitionWithDelegate(wind);
        //GameController.Instance.StartGame();
    }

    public void Start()
    {

    }

    public void Update()
    {

    }
}
