﻿using UnityEngine;
using System.Collections;

public class KillZone : MonoBehaviour
{

    public void OnTriggerEnter(Collider other)
    {
        var thisPlayer = other.GetComponent<PlayerController>();
        if (thisPlayer != null) thisPlayer.Kill("kill zone");
    }
}
