﻿using UnityEngine;
using System.Collections;

public class WinTrigger : MonoBehaviour {
    private bool Won = false;
    public void OnTriggerEnter(Collider other)
    {
        var thisPlayer = other.GetComponent<PlayerController>();
        if (thisPlayer != null && !Won)
        {
            Won = true;
            GameController.Instance.Win(thisPlayer);
        }
    }
}
